﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;

namespace Code_Challenge.EmailConfirmation
{
    public static class EmailHelper
    {
        //If you want to test this please put in your gmail email address and password. Thank you :)
        private const string emailAddress = "";
        private const string emailPassword = "";


        public static string PopulateBody(string firstName, string lastName)
        {
            string body = string.Empty;
            using (StreamReader reader = new StreamReader(HttpContext.Current.Server.MapPath("~/EmailTemplate.html")))
            {
                body = reader.ReadToEnd();

            }
            body = body.Replace("{firstName}", firstName);
            body = body.Replace("{lastName}", lastName);
            return body;
        }
        public static void Send(string mailTo, string body)
        {
           
            try
            {
                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.UseDefaultCredentials = false;
                client.EnableSsl = true;
                client.Credentials = new NetworkCredential(emailAddress, emailPassword);

                MailMessage mailMessage = new MailMessage();
                mailMessage.From = new MailAddress("no-reply@intramedgroupmeetings.com");
                mailMessage.To.Add(mailTo);
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;
             
                mailMessage.Subject = "CONFIRMATION - Speaker Training Meeting";
                client.Send(mailMessage);
            }
            catch (Exception e)
            {

                throw;
            }
        }
    }
}
