﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Register.aspx.cs" Inherits="Code_Challenge.Register" %>

<asp:Content ID="Register" ContentPlaceHolderID="MainContent" runat="server">

       <div class="title">
        CONTACT INFORMATION
    </div>
    <div class="row">
        <ul>
            <li class="li-button">
                <button class="stepper">1</button>
            </li>
            <li>
                <input type="button" class="line"/>
            </li>
            
            <li class="li-button">
                <button class="stepper">2</button>
            </li>
        
             <li>
                <input type="button" class="line" style=" background-color: white;"/>
            </li>
            
            <li class="li-button">
                <button class="stepper" style="background-color:white; color:#1b5875;">3</button>
            </li>
           
        </ul>
        <ul class="bottom-line">
            <li style="font-size:10px; color:#1b5875;">
                REGISTER
            </li>
            
            
            <li style="font-size:10px; color:#1b5875;">
                SUBMIT INFO

            </li>
             
            
             <li style="font-size:10px; color:#1b5875;">
                COMPLETE
            </li>
       
        </ul>
 
      
    </div>
    <div class="jumbotron">
        <label style="font-weight:lighter;">Please fill in the following required information.</label>    <br />

        <asp:Label ID="first_name" runat="server" Text="Label">First Name: *</asp:Label> <br>
        <asp:TextBox ID="firstName"  runat="server" style="width:100%"></asp:TextBox> <br>

        <asp:Label ID="last_name" runat="server" Text="Label">Last Name: * </asp:Label> <br>
        <asp:TextBox ID="lastName"  runat="server" style="width:100%"></asp:TextBox> <br>
   
        <asp:Label ID="state" runat="server" Text="Label">State: *</asp:Label>  <br >
        <asp:DropDownList ID="dropDownState"  class="select" runat="server" >          
            <asp:ListItem Selected="True" value="default">Select One</asp:ListItem>
            <asp:ListItem value="pennsylvania">Pennsylvania</asp:ListItem>
            <asp:ListItem value="new_york">New York</asp:ListItem>
            <asp:ListItem value="new_jersey">New Jersey</asp:ListItem>
            <asp:ListItem value="massachusetts">Massachusetts</asp:ListItem>
        </asp:DropDownList><br>
             
    <label style="font-weight:lighter;">Please provide your email address. <br>
    All meeting correspondence will be sent via email.</label> <br>


    <asp:Label ID="email" runat="server" Text="Label"> Email: *</asp:Label> <br>
    <asp:TextBox ID="txt_email" style="width:100%" runat="server"></asp:TextBox> <br>

    <asp:Label ID="confirm_email" runat="server" Text="Label"> Confirm Email: *</asp:Label>  <br>
    <asp:TextBox ID="confirmEmail" style="width:100%" runat="server"></asp:TextBox><br>

    <asp:CheckBox ID="subscribe" Checked="true"  style="width:15px; height:15px;" runat="server" /> 
    <asp:Label ID="subscription" runat="server" Text="Label"> Subscribe to Newsletter</asp:Label><br>
    

    <asp:Button ID="continuebtn" OnClick="continuebtn_Click" CssClass="button" runat="server" Text="Continue" />
   
    </div>

</asp:Content>
