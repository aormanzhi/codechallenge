﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Code_Challenge.Data.Model
{
    public class Register
    {
        public int RegisterId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string State { get; set; }
        public string Email { get; set; }
        public string ConfirmEmail { get; set; }
        public bool IsSubscribed { get; set; }
    }
}