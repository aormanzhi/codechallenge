﻿using Code_Challenge.Data.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;


namespace Code_Challenge.Data
{
    public class RegisterRepository
    {
        public int Insert(Model.Register register)
        {
            using( var context =  new MyDbContext())
            {
                
                context.RegisterModel.Add(register);
                context.SaveChanges();

            }

            return register.RegisterId;
        }
    }
}