﻿using Code_Challenge.Data.Model;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace Code_Challenge.Data
{
    public class MyDbContext : DbContext
    {
        public virtual DbSet<Model.Register> RegisterModel { get; set; }
        public MyDbContext():base("name=MyDbContext")
        {
            Database.SetInitializer(new NullDatabaseInitializer<MyDbContext>());
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //base.OnModelCreating(modelBuilder);
            
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Entity<Model.Register>().HasKey(p => p.RegisterId);
        }
    }
}