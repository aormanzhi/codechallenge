﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Complete.aspx.cs" Inherits="Code_Challenge.Complete" %>


<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    
     <div class="title">
        COMPLETE
    </div>
       <div class="row">
        <ul>
            <li class="li-button">
                <button class="stepper">1</button>
            </li>
            <li>
                <input type="button" class="line"/>
            </li>
            
            <li class="li-button">
                <button class="stepper">2</button>
            </li>
        
             <li>
                <input type="button" class="line-complete" />
            </li>
            
            <li >
                <button class="li-button-complete" >3</button>
            </li>
           
        </ul>
        <ul class="bottom-line">
            <li style="font-size:10px; color:#1b5875;">
                REGISTER
            </li>
            
            
            <li style="font-size:10px; color:#1b5875;">
                SUBMIT INFO

            </li>
             
            
             <li style="font-size:10px; color:#1b5875;">
                COMPLETE
            </li>
       
        </ul>
 
      
    </div>
    <div class="jumbotron">
         <h4 style="font-weight:bold"> Thank you for registering!</h4>
    <p style="font-size:16px;">You should receive a confirmation email momentarily containing additional details.</p>
    </div>
   
   
</asp:Content>
