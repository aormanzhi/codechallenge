﻿<%@ Page Language="C#"  MasterPageFile="~/Site.Master"  AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="Code_Challenge.Login" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div class="title">
        LOGIN
    </div>

    <div class="jumbotron">
         <div>
             <asp:Label style="font-weight:lighter;" ID="Label1" runat="server" Text="Label">Please enter the case sensitive password from your official invitation.</asp:Label>    <br>
     
             <asp:Label ID="Label2"  runat="server" Text="Label">Password:</asp:Label>   <br>

             <asp:TextBox ID="password" style="width:100%" name="password" runat="server"></asp:TextBox>   <br><br>  
            
             <asp:Button ID="login" CssClass="button" OnClick="submit_Click" runat="server" Text="Login" />
        </div>
    </div>
</asp:Content>
