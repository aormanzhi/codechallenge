﻿using Code_Challenge.Data;
using Code_Challenge.EmailConfirmation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Code_Challenge
{
    public partial class Register : System.Web.UI.Page
    {
        protected void continuebtn_Click(object sender, EventArgs e)
        {

            RegisterRepository repo = new RegisterRepository();
            Data.Model.Register register = new Data.Model.Register
            {
                FirstName = firstName.Text,
                LastName = lastName.Text,
                State = dropDownState.SelectedValue,
                Email = txt_email.Text,
                ConfirmEmail = confirmEmail.Text
            };
            repo.Insert(register);

            string body = EmailHelper.PopulateBody(register.FirstName, register.LastName);
            EmailHelper.Send(register.Email, body);

            Response.Redirect("RegisterValidationError.aspx");

        }
    }
}